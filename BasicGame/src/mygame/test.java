/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.app.SimpleApplication;
import com.jme3.audio.AudioNode;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.post.FilterPostProcessor;
import com.jme3.renderer.queue.RenderQueue;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.debug.WireFrustum;
import com.jme3.scene.shape.Box;
import com.jme3.scene.shape.Sphere;
import com.jme3.shadow.BasicShadowRenderer;
import com.jme3.shadow.PssmShadowRenderer;
import com.jme3.util.SkyFactory;
import com.jme3.water.WaterFilter;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

/**
 *
 * @author devilbeny
 */
public class test extends SimpleApplication implements ActionListener, ScreenController {

    BasicShadowRenderer bsr;
    private Spatial mapaModel;
    private BulletAppState bulletAppState;
    private RigidBodyControl graneroCape;
    private CharacterControl player;
    private Vector3f walkDirection = new Vector3f();
    private boolean left = false, right = false, up = false, down = false;
    private AudioNode audio_ambient;
    private Nifty nifty;
    private Box myBoxShape;
    private Geometry myBox;
    private float myBox_speed = 0f;
    private float myBox_acceleration = 2f;
    private Spatial graneroModel;
    private CollisionShape mapaShape;
    private CollisionShape graneroShape;
    private RigidBodyControl mapaCape;
    private WireFrustum frustum;
    private PssmShadowRenderer pssmRenderer;
    private Spatial tractorModel;
    private CollisionShape tractorShape;
    private RigidBodyControl tractorCape;
    private Spatial casaCModel;
    private CollisionShape casaCShape;
    private RigidBodyControl casaCCape;
    private Spatial autoCModel;
    private CollisionShape autoCShape;
    private RigidBodyControl autoCCape;
    private Spatial arbolModel;
    Node shootables;
    Geometry mark;
    private WaterFilter water2;
    private Vector3f lightDir = new Vector3f(-.5f, -.5f, -.5f);
    private Spatial grassModel;
    private CollisionShape arbolShape;
    private RigidBodyControl arbolCape;
    private Spatial mapaModel2;

    // MAIN 
    public static void main(String[] args) {
        test app = new test();
        app.start();
    }
    //metodo principal

    public void simpleInitApp() {

        //  setDisplayFps(false);
        //setDisplayStatView(false);
        bulletAppState = new BulletAppState();
        stateManager.attach(bulletAppState);
        //viewPort.setBackgroundColor(new ColorRGBA(0.7f, 0.8f, 1f, 1f));
        flyCam.setMoveSpeed(10f);
        setUpKeys();
        setUpLight();
        initCrossHairs(); // a "+" in the middle of the screen to help aiming
        // initKeys();       // load custom key mappings
        initMark();       // a red sphere to mark the hit
        initMap();

    }
    private ActionListener actionListener = new ActionListener() {
        public void onAction(String name, boolean keyPressed, float tpf) {
            if (name.equals("Shoot") && !keyPressed) {
                // 1. Reset results list.
                CollisionResults results = new CollisionResults();
                // 2. Aim the ray from cam loc to cam direction.
                Ray ray = new Ray(cam.getLocation(), cam.getDirection());
                // 3. Collect intersections between Ray and Shootables in results list.
                shootables.collideWith(ray, results);
                // 4. Print the results
                System.out.println("----- Collisions? " + results.size() + "-----");
                for (int i = 0; i < results.size(); i++) {
                    // For each hit, we know distance, impact point, name of geometry.
                    float dist = results.getCollision(i).getDistance();
                    Vector3f pt = results.getCollision(i).getContactPoint();
                    String hit = results.getCollision(i).getGeometry().getName();
                    System.out.println("* Collision #" + i);
                    System.out.println("  You shot " + hit + " at " + pt + ", " + dist + " wu away.");
                }
                // 5. Use the results (we mark the hit object)
                if (results.size() > 0) {
                    // The closest collision point is what was truly hit:
                    CollisionResult closest = results.getClosestCollision();
                    // Let's interact - we mark the hit with a red dot.
                    mark.setLocalTranslation(closest.getContactPoint());
                    rootNode.attachChild(mark);
                } else {
                    // No hits? Then remove the red mark.
                    rootNode.detachChild(mark);
                }
            }
        }
    };

    protected void initMark() {
        Sphere sphere = new Sphere(30, 30, 0.2f);
        mark = new Geometry("BOOM!", sphere);
        Material mark_mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mark_mat.setColor("Color", ColorRGBA.Red);
        mark.setMaterial(mark_mat);
    }

    protected void initCrossHairs() {
        setDisplayStatView(false);
        guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
        BitmapText ch = new BitmapText(guiFont, false);
        ch.setSize(guiFont.getCharSet().getRenderedSize() * 2);
        ch.setText("> <"); // crosshairs
        ch.setLocalTranslation((cam.getWidth() / 2) - 25, (cam.getHeight() / 2) + 25, 0);
        guiNode.attachChild(ch);
    }

    private void setUpLight() {
        // We add light so we see the scene

        AmbientLight al = new AmbientLight();
        al.setColor(ColorRGBA.White.mult(1.3f));
        rootNode.addLight(al);

        DirectionalLight sun = new DirectionalLight();
        sun.setColor(ColorRGBA.White);
        sun.setDirection(lightDir);
        rootNode.addLight(sun);

        pssmRenderer = new PssmShadowRenderer(assetManager, 1024, 3);
        pssmRenderer.setDirection(new Vector3f(lightDir).normalizeLocal()); // light direction
        viewPort.addProcessor(pssmRenderer);

    }

    private void setUpKeys() {

        inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_S));
        inputManager.addMapping("Jump", new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addListener(this, "Left");
        inputManager.addListener(this, "Right");
        inputManager.addListener(this, "Up");
        inputManager.addListener(this, "Down");
        inputManager.addListener(this, "Jump");
        inputManager.addMapping("Shoot", new MouseButtonTrigger(MouseInput.BUTTON_LEFT)); // trigger 2: left-button click
        inputManager.addListener(actionListener, "Shoot");
    }

    public void bind(Nifty nifty, Screen screen) {
        System.out.println("bind( " + screen.getScreenId() + ")");
    }

    public void onStartScreen() {
        System.out.println("onStartScreen");
        stateManager.detach(bulletAppState);
        flyCam.setRotationSpeed(0);

    }

    public void onEndScreen() {
        System.out.println("onEndScreen");


    }

    public void gotoScreenTwo() {
        nifty.gotoScreen("screenTwo");

    }

    public void quit() {
        inputManager.setCursorVisible(false);
        stateManager.attach(bulletAppState);
        flyCam.setRotationSpeed(10);
        nifty.gotoScreen("end");
    }

    public void onAction(String binding, boolean value, float tpf) {
        if (binding.equals("Left")) {
            if (value) {
                left = true;
            } else {
                left = false;
            }
        } else if (binding.equals("Right")) {
            if (value) {
                right = true;
            } else {
                right = false;
            }
        } else if (binding.equals("Up")) {
            if (value) {
                up = true;
            } else {
                up = false;
            }
        } else if (binding.equals("Down")) {
            if (value) {
                down = true;
            } else {
                down = false;
            }
        } else if (binding.equals("Jump")) {
            player.jump();
        }
    }

    @Override
    public void simpleUpdate(float tpf) {

        //corazonModel.rotate(0, tpf * 0.25f, 0);

        Vector3f camDir = cam.getDirection().clone().multLocal(0.6f);
        Vector3f camLeft = cam.getLeft().clone().multLocal(0.4f);
        walkDirection.set(0, 0, 0);
        if (left) {
            walkDirection.addLocal(camLeft);
        }
        if (right) {
            walkDirection.addLocal(camLeft.negate());
        }
        if (up) {
            walkDirection.addLocal(camDir);
        }
        if (down) {
            walkDirection.addLocal(camDir.negate());
        }
        player.setWalkDirection(walkDirection);
        cam.setLocation(player.getPhysicsLocation());


    }

    private void initMap() {

        FilterPostProcessor fpp = new FilterPostProcessor(assetManager);

        water2 = new WaterFilter(rootNode, lightDir);
        water2.setCenter(new Vector3f(0, 0, 0));
        water2.setRadius(260);
        water2.setWaterHeight(0.8f);
        water2.setUseFoam(false);
        water2.setUseRipples(false);
        water2.setDeepWaterColor(ColorRGBA.Brown);
        water2.setWaterColor(ColorRGBA.Brown.mult(2.0f));
        water2.setWaterTransparency(0.2f);
        water2.setMaxAmplitude(0.3f);
        water2.setWaveScale(0.008f);
        water2.setSpeed(0.7f);
        water2.setShoreHardness(1.0f);
        water2.setRefractionConstant(0.2f);
        water2.setShininess(0.3f);
        water2.setSunScale(1.0f);
        water2.setColorExtinction(new Vector3f(10.0f, 20.0f, 30.0f));
        fpp.addFilter(water2);


        viewPort.addProcessor(fpp);


        //inicia incrustado de objetos
//pasto
        grassModel = assetManager.loadModel("Models/Grass/grass.j3o");
        grassModel.setLocalScale(2.5f);
        grassModel.setLocalTranslation(0f, 3f, 26f);
        rootNode.attachChild(grassModel);
        grassModel.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);
//Arbol
        arbolModel = assetManager.loadModel("Models/arbol/arbol.mesh.j3o");
        arbolModel.setLocalScale(3.5f);
        arbolModel.setLocalTranslation(0f, 3f, 26f);
        rootNode.attachChild(arbolModel);
        arbolModel.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);
        arbolShape = CollisionShapeFactory.createMeshShape((Node) arbolModel);
        arbolCape = new RigidBodyControl(arbolShape, 0);
        arbolModel.addControl(arbolCape);
//auto
        autoCModel = assetManager.loadModel("Models/ClasicCar/clasicCar.mesh.j3o");
        autoCModel.setLocalScale(3.9f);
        autoCModel.setLocalTranslation(1f, 3f, 21f);
        rootNode.attachChild(autoCModel);
        autoCModel.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);
        autoCShape = CollisionShapeFactory.createMeshShape((Node) autoCModel);
        autoCCape = new RigidBodyControl(autoCShape, 0);
        autoCModel.addControl(autoCCape);
//casa
        casaCModel = assetManager.loadModel("Models/casaC/HouseC.mesh.j3o");
        casaCModel.setLocalScale(1.3f);
        casaCModel.setLocalTranslation(-15f, 3f, 22f);
        rootNode.attachChild(casaCModel);
        casaCModel.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);
        casaCShape = CollisionShapeFactory.createMeshShape((Node) casaCModel);
        casaCCape = new RigidBodyControl(casaCShape, 0);
        casaCModel.addControl(casaCCape);
//tractor
        tractorModel = assetManager.loadModel("Models/tractor/tractor.mesh.j3o");
        tractorModel.setLocalScale(1.5f);
        tractorModel.setLocalTranslation(-5f, 3f, -5f);
        rootNode.attachChild(tractorModel);
        tractorModel.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);
        tractorShape = CollisionShapeFactory.createMeshShape((Node) tractorModel);
        tractorCape = new RigidBodyControl(tractorShape, 0);
        tractorModel.addControl(tractorCape);
//granero
        graneroModel = assetManager.loadModel("Models/Granero/Granero.j3o");
        graneroModel.setLocalScale(1.7f);
        graneroModel.setLocalTranslation(0f, 3f, -6f);
        rootNode.attachChild(graneroModel);
        graneroModel.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);
        graneroShape = CollisionShapeFactory.createMeshShape((Node) graneroModel);
        graneroCape = new RigidBodyControl(graneroShape, 0);
        graneroModel.addControl(graneroCape);
//terreno
        mapaModel = assetManager.loadModel("Scenes/myTest.j3o");
        mapaModel.setLocalTranslation(0f, 3f, 0f);
        rootNode.attachChild(mapaModel);
        mapaModel.setShadowMode(RenderQueue.ShadowMode.Receive);
        mapaShape = CollisionShapeFactory.createMeshShape((Node) mapaModel);
        mapaCape = new RigidBodyControl(mapaShape, 0);
        mapaModel.addControl(mapaCape);
//copia del primer terreno con pasto, sin existencia fisica del terreno, solo el grass
        mapaModel2 = assetManager.loadModel("Scenes/myTest_1.j3o");
        mapaModel2.setLocalTranslation(0f, 2f, 0f);
        rootNode.attachChild(mapaModel2);
        mapaModel2.setShadowMode(RenderQueue.ShadowMode.CastAndReceive);
        
//Player y colicion del player
        CapsuleCollisionShape capsuleShape = new CapsuleCollisionShape(1f, 1f, 1);
        player = new CharacterControl(capsuleShape, 0.05f);
        player.setJumpSpeed(20);
        player.setFallSpeed(30);
        player.setGravity(60);
        player.setPhysicsLocation(new Vector3f(6, 4, 6));


        //SOlides

        bulletAppState.getPhysicsSpace().add(arbolCape);
        bulletAppState.getPhysicsSpace().add(autoCCape);
        bulletAppState.getPhysicsSpace().add(casaCCape);
        bulletAppState.getPhysicsSpace().add(tractorCape);
        bulletAppState.getPhysicsSpace().add(graneroCape);
        bulletAppState.getPhysicsSpace().add(mapaCape);
        bulletAppState.getPhysicsSpace().add(player);


        //shoot
        shootables = new Node("Shootables");
        rootNode.attachChild(shootables);
        shootables.attachChild(mapaModel);
        shootables.attachChild(graneroModel);
        shootables.attachChild(autoCModel);
        shootables.attachChild(casaCModel);
        shootables.attachChild(tractorModel);



        NiftyJmeDisplay niftyDisplay = new NiftyJmeDisplay(assetManager,
                inputManager,
                audioRenderer,
                guiViewPort);
        nifty = niftyDisplay.getNifty();

        guiViewPort.addProcessor(niftyDisplay);
        inputManager.setCursorVisible(true);


        //Sky al root

        rootNode.attachChild(SkyFactory.createSky(assetManager, "Textures/BrightSky.dds", false));

    }
}
